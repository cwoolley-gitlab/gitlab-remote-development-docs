# Remote Development Developer Documentation

[[_TOC_]]

## Overview

This repo is a place for early iteration on developer-facing documentation related to the [Remote Development](https://docs.gitlab.com/ee/user/project/remote_development/) feature.

The documentation here will probably eventually be moved to other locations, such as the [`gitlab-agent` project's `doc` folder](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/doc),
or the [Remote Development Architecture Blueprint](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/architecture/blueprints/remote_development/index.md).

For now, though, it provides a place to capture and collaborate on early notes/docs, in source control.

## Architecture

See [Architecture](doc/architecture.md).

## Feature Implementation Details

See [Feature Implementation Details](doc/feature-implementation-details.md).

## Local Development Environment Setup

See [Local Development Environment Setup](doc/local-development-environment-setup.md).

## Cloud Sandbox Environment Setup

See [Cloud Sandbox Environment Setup](doc/cloud-sandbox-environment-setup.md).

## Securing the Workspace

See [Securing the Workspace](doc/securing-the-workspace.md).
