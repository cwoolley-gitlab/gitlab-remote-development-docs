# Local Development Environment Setup

[[_TOC_]]

## Configure Kubernetes locally

- Install [Rancher.Desktop-1.6.2.aarch64.dmg](https://github.com/rancher-sandbox/rancher-desktop/releases/tag/v1.6.2)
- Create a Kubernetes cluster `v1.25.3` with `contrainerd` container engine and uncheck the default selected `Enable Traefik` option

### Configure a load balancer

```shell
# install ingress-nginx
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update
helm install \
ingress-nginx ingress-nginx/ingress-nginx \
--namespace ingress-nginx \
--create-namespace \
--version 4.3.0

export LOAD_BALANCER_EXTERNAL_IP=$(
kubectl -n ingress-nginx \
get svc ingress-nginx-controller \
--output jsonpath='{.status.loadBalancer.ingress[0].ip}'
)
```

### Configure Dependency - Cert Manager

```shell
export CERT_MANAGER_NAMESPACE=cert-manager
export CERT_MANAGER_GIT_TAG=v1.10.0
kubectl apply -f "https://github.com/cert-manager/cert-manager/releases/download/${CERT_MANAGER_GIT_TAG}/cert-manager.crds.yaml"

helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install \
cert-manager jetstack/cert-manager \
--namespace ${CERT_MANAGER_NAMESPACE} \
--create-namespace \
--version ${CERT_MANAGER_GIT_TAG}
```

### Install DevWorkspace Operator

```shell
export DEVWORKSPACE_OPERATOR_GIT_URL_RAW=https://raw.githubusercontent.com/devfile/devworkspace-operator
export DEVWORKSAPCE_OPERATOR_GIT_TAG=v0.17.0
export DEVWORKSAPCE_OPERATOR_BASE_URL=${DEVWORKSPACE_OPERATOR_GIT_URL_RAW}/${DEVWORKSAPCE_OPERATOR_GIT_TAG}
export NAMESPACE=devworkspace-controller
export DWO_IMG=quay.io/devfile/devworkspace-controller:v0.17.0
export PULL_POLICY=Always
export DEFAULT_ROUTING=basic
export ROUTING_SUFFIX=remotedev.${LOAD_BALANCER_EXTERNAL_IP}.nip.io

kubectl create ns ${NAMESPACE}
kubectl apply -f ${DEVWORKSAPCE_OPERATOR_BASE_URL}/deploy/deployment/kubernetes/combined.yaml
curl ${DEVWORKSAPCE_OPERATOR_BASE_URL}/deploy/default-config.yaml | envsubst | kubectl apply -f -

# ensure all pods are ready and in running state
kubectl -n ${NAMESPACE} get po --watch
```

### Verify everything is working

The [examples in this repo](../examples) have been sourced from this [demo](https://github.com/l0rd/devworkspace-demo)

```shell
# create resources
kubectl apply -f ./examples

# browse the URL presented in the below output
kubectl get dw -n default --watch

# delete resources
kubectl delete -f ./examples
```

### Sync Load Balancer External IP and DevWorkspaceOperatorConfig Routing Suffix

During your local development, you will restart your machine and your kubernetes cluster multiple times. This might cause the Load Balancer External IP to change. This needs to be synced in the routing suffix of DevWorkspaceOperatorConfig. This can be achieved by running the following command -

```sh
export RD_LOCAL_DEVELOPMENT_NAMESPACE="rd-local-development"
kubectl create namespace ${RD_LOCAL_DEVELOPMENT_NAMESPACE}
kubectl apply -f ./examples/6-rd-local-development-ingress-dwoc-sync.yaml
```

To see the logs of this application

```sh
kubectl logs -f -n ${RD_LOCAL_DEVELOPMENT_NAMESPACE} -l "app=ingress-dwoc-sync"
```

## Configure GDK to use GitLab agent for Kubernetes (ga4k)

1. [Install GDK](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main#supported-methods).
1. [Set up an EE license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee-developer-licenses).
1. Follow [doc/howto/local_network.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/local_network.md#local-network-binding) to set up your GDK to run on `172.16.123.1` IP address.
1. Follow [doc/howto/kubernetes_agent.md](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/kubernetes_agent.md) to enable ga4k in your GDK.
1. [Register an agent in your GDK](https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#register-the-agent-with-gitlab). Use any project. Registering an agent generates an **agent token**. You need this token to start an agent.

## Run GA4K locally (both kas and agentk)

`kas` is a server component that runs next to the GitLab, `agentk` is a client component that runs inside k8s cluster. For more info see the [architecture docs](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md).

Follow the [Running kas and agentk locally](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/developing.md#running-kas-and-agentk-locally) section of agent development docs. Use your agent token instead of `<TOKEN>`.
