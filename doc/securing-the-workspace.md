# Securing the Workspace

[[_TOC_]]

A workspace is a container/VM based developer machines providing all the tools
and dependencies needed to code, build, test, run, and debug applications.
This sections aims to answer the question - how do we secure a workspace?

### Problem Statement

* How to ensure that users are able to access only their own workspaces?
* How to ensure that all traffic entering the workspace on any port is
authenticated and authorized?
* While building inside a workspace, a developer may start a server which they
would want to access from outside the workspace(e.g. to see the server's output
through a browser URL). How do we protect all ports in a workspace but also
dynamically configure them based on user action?

### Proposal

We should run a proxy inside each workspace which will perform some action
(authentication/authorization/verification) and forward the request to the application.
We shall refer this proxy as `workspace-proxy` to uniquely identify it and not confuse
it with other proxies.

![workspace-proxy-non-transparent](../images/workspace-proxy-non-transparent.png)

The challenge here is how do we run this workspace-proxy transparently i.e. if the application
is running inside the workspace at port 3000 and it is being accessed from outside
the workspace on port 3000, how do we run a workspace-proxy which will intercept this traffic,
perform some action and transparently forward traffic to the application running
on port 3000? If we don't do it transparently, we'll have to run N proxies for
protecting N ports or figure out ways to do port-mapping between workspace-proxy port and
application port. This quickly becomes a challenge since running running N workspace-proxies
is a scalability problem and performing some form of port-mapping limits the number of ports
available to the end user and thus would not impact the native development experience.

Using firewall rules is the oldest trick in IT industry to control traffic flow.
One such mechanism to modify firewall rules is
[`iptables`](https://manpages.debian.org/unstable/iptables/iptables.8.en.html).

Each VM or a Pod in Kubernetes gets access to at least 2 network interfaces
i.e. `lo` and `eth0`. Interface `lo` stands for loopback and `eth0` for ethernet.
`iptables` are used to map one port to another.
The idea here is map the application port (3000) on the `eth0` interface to
the workspace-proxy port (8000). This will ensure that the traffic from outside the VM/Pod
is routed to the workspace-proxy whenever it tries to access the application via port 3000.
We let the `lo` interface to route VM/Pod-internal traffic directly to the
destination application.

![workspace-proxy-transparent](../images/workspace-proxy-transparent.png)

### Proof of concept

For VM, cloud providers already allow a way to specify custom script to be run on
initialization. In Kubernetes, you can run init containers prior to running regular
containers in a Pod.

For this proof of concept, we'll focus on Kubernetes.
We use an init container to set up the Pod networking in order
to set up the necessary `iptables` rules. This init container needs to be run as
`root` user but all other containers are run as `non-root` user.

We are using `iptables` to route incoming taffic on port 3000 and port 5000 on
`eth0` to port `8000` through an `init container`. A workspace-proxy is running on port 8000 and
applications are running on port 3000 and 5000 each. The workspace-proxy extract the port from
the host and forwards it to the application on the extracted port(3000 or 5000).
It then writes the response back and appends a header
`X-GitLab-Workspace-Proxy: gitlab-remote-development-workspace-proxy` to identify
that the traffic has been served by it.

The workspace-proxy code can be viewed
[here](https://gitlab.com/vtak/gitlab-remote-development-workspace-proxy).

We have even integrated the above workspace-proxy in our solution.

```shell
# create the workspace-proxy devworkspacetemplate
kubectl apply -f ./examples/0-workspace-proxy.yaml

# create a workspace which has this workspace-proxy injected
kubectl apply -f ./examples/5-dw-with-proxy-ttyd.yaml
```

Verify workspace-proxy is working as expected

```shell
export WORKSPACE_ID=$(
    kubectl get dw dw-with-proxy-ttyd -o jsonpath="{.status.devworkspaceId}"
)
export INGRESS_HOST_1=$(
    kubectl get ing -l controller.devfile.io/devworkspace_id==${WORKSPACE_ID} | \
    grep httpbin1-3000 | \
    tr -s ' ' | \
    cut -d ' ' -f3
)
export INGRESS_HOST_2=$(
    kubectl get ing -l controller.devfile.io/devworkspace_id==${WORKSPACE_ID} | \
    grep httpbin2-5000 | \
    tr -s ' ' | \
    cut -d ' ' -f3
)

# check logs of workspace-proxy-init-networking
kubectl logs -f ${POD_NAME} -c workspace-proxy-init-networking

# tail logs of workspace-proxy
kubectl logs -f ${POD_NAME} -c workspace-proxy

# send traffic on port 3000 and 5000
# verify "X-GitLab-Workspace-Proxy" header is injected.
curl -i "${INGRESS_HOST_1}/get?query=param"
curl -i "${INGRESS_HOST_2}/get?query=param"
```

### Where do we go from here

#### Add security context to Pod

A [security context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)
defines privilege and access control settings for a Pod or Container.
There are multiple settings that we should explore and work on.

#### Harden all container images being used

We should aim to use [`distroless`](https://github.com/GoogleContainerTools/distroless)
container images for all the components that we build/use so that
the attack surface and exposure to bugs and vulnerabilities is reduced.

"Distroless" images contain only your application and its runtime dependencies.
They do not contain package managers, shells or any other programs you would expect to
find in a standard Linux distribution.

Additional benefit is that Distroless images are very small. Thus, it would help in
pulling images faster, reducing bandwidth consumption and saving network costs.

Since distroless images do not include a shell or any debugging utilities,
it's difficult to troubleshoot distroless images using `kubectl exec` alone.

#### Isolate each user with a Kubernetes Namespace

Each user should have a unique namespace in Kubernetes. This allows us to isolate
users more easily in Kubernetes. Any user specific resources(e.g. user's workspace preferences,
user secrets, etc.) can be shared across mutliple workspaces
of the user in the same namespace. It also allows us to add any other policies like
[Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
more easily.

#### Block all traffic to the workspace by default

Currently, all traffic is allowed to enter the workspace. We should tackle this by
configuring rules to deny all traffic and only allow the ones which we desire.

This can be achieved through

1. `iptables` rules

    Something along the lines of the following should be considered. Of course, we'll
    have to figure out the right combination of rules to apply on workspace creation/startup.
    This would work for both Kubernetes and non-Kubernetes workloads.

    ```shell
    # Set default chain policies
    iptables -P INPUT DROP
    iptables -P FORWARD DROP
    iptables -P OUTPUT ACCEPT

    # Accept on localhost
    iptables -A INPUT -i lo -j ACCEPT
    iptables -A OUTPUT -o lo -j ACCEPT

    # Forward traffic on desired ports to the workspace-proxy
    ...
    ```

    There is further scope for only allowing certain egress traffic based on customer's
    organizational and compliance need.

1. Kubernetes Network Policies

    Kubernetes [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/)
    are an application-centric construct which allow you to specify how a pod
    is allowed to communicate with various network entities over the network.

    The entities that a Pod can communicate with are identified through a combination of the
    following 3 identifiers:

    1. Other pods that are allowed (exception: a pod cannot block access to itself)
    1. Namespaces that are allowed
    1. IP blocks (exception: traffic to and from the node where a Pod is running
    is always allowed, regardless of the IP address of the Pod or the node)

    However, Network policies are implemented by the Kubernetes network plugin.
    To use network policies, you must be using a networking solution which supports NetworkPolicy.
    Creating a NetworkPolicy resource without a controller that implements it will have no effect.

We can use either `iptables` or `Kubernetes Network Policies` or use both of them for multi-layer
security.

#### Modifying iptables rules dynamically

While building inside a workspace, a developer may start a server which they
would want to access from outside the workspace(e.g. to see the server's output
through a browser URL). In such a case, we would have to modify the `iptables` rules to allow
traffic on certain ports. Some ways to tackle this are

1. Instead of running the `init container` to modify the `iptables` rules, run it as an
additional `sidecar container` as `root` user which will watch for any new ports to act on.
1. Run the workspace-proxy component as `root` user with additional process running alongside it
which updates the `iptables` rules. This would mean there is no need for any
additional `init container` or additional `sidecar container`.
1. Create a new Pod as `root` user which will modify the `iptables` rules for
an existing workspace pod. Something along the lines of querying the Kubernetes node to
figure out which ethernet is attached to the workspace and then modify the rules
for that ethernet.
1. Run the `iptables` rules modification as `init container` on startup and
[`ephemeral containers`](https://kubernetes.io/docs/concepts/workloads/pods/ephemeral-containers/)
dynamically when required. Ephemeral container is a special type of container that
runs temporarily in an existing Pod to accomplish certain actions. However, you use
ephemeral containers to inspect services rather than to build applications.

#### Authentication and Authorization

Currently, the workspace-proxy is only accepting the traffic, forwarding it and then responding
back to the client. Following are the ways in which we can add authentication and authorization
to this flow.

1. Authentication and authorization in workspace-proxy

    Any changes to authentication and authorization logic would require restarting the existing
    workspaces. This can become challenging.

1. Authentication in ingress controller and authorization in workspace-proxy

    Use [OAuth annotations on ingress-nginx](https://kubernetes.github.io/ingress-nginx/examples/auth/oauth-external-auth/)
    to perform authentication. Add authorization logic in workspace-proxy.

    Any changes to authorization logic would require restarting the existing
    workspaces. This can become challenging.

1. Authentication and authorization in ingress-nginx through GA4K
and signed header verification in workspace-proxy

    Add authentication and authorization endpoints in GA4K agent_module. Add annotations on ingress-nginx
    to use these endpoints. [example](https://github.com/kubernetes/ingress-nginx/tree/3474c33e15d809ba401b38891a2ed3c4080b751e/docs/examples/customization/external-auth-headers).
    These endpoints will add verifiable signed headers which will be verified in workspace-proxy.
    The workspace-proxy will drop(and possibly report in the future) any traffic which does not
    have correctly signed headers.

    Limiting traffic through authentication and authorization at ingress layer restricts traffic
    at the earliest.

    Any changes to authentication and authorization logic would require updating the GA4K agent_module.
    Whether this should be further optimized by moving the logic to GA4K server_module or a separate
    application which we can deploy in the Kubernetes cluster directly is something we'll
    have to consider.

### Build the workspace-proxy

Currently, the workspace-proxy simply handles HTTP connections to showcase a proof-of-concept.
We will have to make it a reliable proxy which can handle different types of connections,
upgrades, etc. What logic should reside in this workspace-proxy will be driven by the
decisions made above this section.

#### Explore other ideas

* [Pod Security Standards](https://kubernetes.io/docs/concepts/security/pod-security-standards/)

### Questions

* What are the challenges while modifying firewall rules using `iptables`? Both for Kubernetes
and non-Kubernetes workloads.
* Is there a better way to modify firewall rules than `iptables`? Maybe `nftables` or `ufw`?
* Any concerns with intercepting traffic and filtering it from an enterprise customer perspective?
* Is there a better way to create an air-tight workspace than to modify firewall rules?

### References

- https://venilnoronha.io/hand-crafting-a-sidecar-proxy-and-demystifying-istio
- https://gitlab.com/vtak/gitlab-remote-development-workspace-proxy
