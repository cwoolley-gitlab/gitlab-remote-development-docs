# Architecture

[[_TOC_]]

NOTE: This document is currently in draft mode and is being actively developed.

This is not intended to be the final architecture - it is a minimal viable architecture
to meet the current requirements.

There will be a number of iterations on this architecture as we learn more about
requirements such as performance and scalability.

## TODO

Notes for updates from recent discussions:

- Switch to consistent terminology of `actual_state` for agentk -> rails requests, and `desired_state` for rails -> agentk response.
- Switch from using timestamps for k8s state marker to using monotonically-increasing `resourceVersion` integer from k8s informers.
- Add new workspace states:
  - "Unknown" - agentk restarted and does not have any cache, rails should respond by sending all of the workspace versions it has.
  - "Invalid" - the last response received from Rails for this workspace was invalid. E.g., a devworkspace config YAML which was syntactically incorrect and could not be successfully applied

## Overview

This page contains architecture documentation for the Remote Development feature.

It consists primarily of UML diagrams which represent the architecture from different aspects
levels of detail.

These diagrams are intended to help align the team for the initial spike implementations of
this architecture. They will certainly change and evolve over time,
as we continue to refine the architecture.

## GitLab Agent for Kubernetes Remote Development Module Architecture

The overall goal of this architecture is to ensure that the **_desired state_** of all
Remote Development workspaces is represented by the **_actual state_** of the
workspaces running in the Kubernetes clusters.

This is accomplished as follows

1. The desired state of the workspaces is obtained from user actions in the GitLab UI or API and
   persisted in the Rails database
2. There is a polling loop which:
   1. Retrieves the actual state of the workspaces from the Kubernetes clusters and sends it to Rails
      to be persisted
   2. Rails compares the actual state with the desired state and initiates any workspace status actions to
      bring the actual state in line with the desired state.

### Terminology

These are some terms which are used in the following UML diagrams.

**NOTE: Some of these terms or naming conventions may change in the future**

`Rails`:

* This is our Rails monolith, exposing a REST API and having a connection to the Postgres DB.
* The business logic would reside in the Rails monolith
  * Right now, business logic includes transforming devfile to a devworkspace k8s resource, injecting the editor inside it, etc.
  * Eventually, business logic will include configuring resources based on which tier the user belongs to. e.g. If the user is free, maximum allowed CPU/RAM usage is x units.

`Postgres`:

* This is the Postgres database in the Rails monolith, where the state of all the remote development workspaces
  is persisted.
* There will be a single `workspaces` table, with one row to represent each workspace.

`Gitaly`

* Some data may be retrieved directly from Gitaly, e.g. the contents of the devfile config for a workspace.

`GitLab Agent for Kubernetes` (`GA4K`)

See the following docs for `GA4K`:
* User docs: https://docs.gitlab.com/ee/user/clusters/agent/index.html
* Developer docs: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/tree/master/doc
* Architecture docs: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/architecture.md

`GA4K Server` (`kas`)

* The "server" component of `GA4K`. Single service of `kas` runs colocated with Rails (service has one configuration, but can have multiple replicas).

`GA4K Agent` (`agentk`)

* The "agent" component of `GA4K`. Multiple services of `agentk` run on the Kubernetes cluster, and each
  agent service can run multiple pods.

`kas` `remote_dev` module

* In the initial architecture, the polling requests to rails will be initiated by `agentk`, and `kas` will
  merely act as a proxy to rails. Therefore, there will be little or `remote_dev`-specific code in `kas`
  in this initial architecture. This may change in the future as part of performance, scalability, or
  other optimizations.

`agentk` `remote_dev` module

* It has a regular polling loop.
* It has access to the Kubernetes API, and listens for changes related to workspaces.
* It can make REST POST requests to Rails, by proxying them through `kas` 
* On each poll, it sends a POST request to the Rails REST API with data representing the actual state of the workspaces
  in the request body, and receives any new desired state to be applied from Rails in the response body.

`Kubernetes API` (`k8s`)

* The standard Kubernetes API server with DevWorkspace Operator installed. The DevWorkspace Operator will
  be responsible for applying the necessary workspace status actions to the Kubernetes cluster to make the actual state
  match the desired state.

### Network Topology

Notes:

- The Kubernetes API is not shown in this diagram, but it is assumed that it is managing the workspaces via the 
DevWorkspace Operator being invoked by `agentk`.
- The numbers of components in each cluster are arbitrary.

```plantuml
@startuml

node "GitLab Monolith" as gitlab {
  rectangle rails
  database postgres
  rectangle "kas deployment" as kas_deployment {
      collections kas1..kas8
  }
}

cloud cloud

cloud -left- kas1..kas8
kas_deployment -left- rails 
rails -left- postgres 

node "kubernetes cluster" as k8s {
  rectangle "agentk A workspaces" as agentka_workspaces {
      collections workspace2..workspace8
      rectangle workspace1
  }
  
  rectangle "agentk B workspaces" as agentk2..agentk8_workspaces {
      collections workspace10..workspace16
      rectangle workspace9
  }
  
  rectangle "agentk deployment A" as agentk_a_deployment {
      rectangle agentk_a_1
  }

  rectangle "agentk deployment B" as agentk_b_deployment {
      collections agentk_b_1..agentk_b_8
  }

  agentk_a_1 -right- agentka_workspaces
  agentk_b_1..agentk_b_8 -right- agentk2..agentk8_workspaces
}

'the following hidden line is a hack to get the diagram to render correctly
agentk_a_1 -[hidden]- agentk_b_1..agentk_b_8
cloud -right- agentk_a_1
cloud -right- agentk_b_1..agentk_b_8


@enduml

```

### Workspace management APIs and messaging

All requests/responses between agentk and rails are synchronous.

#### Types of messages

Agentk can send different types of messages to rails to capture/receive different information. Different types of messages are -

* `prerequisites` - Fetch k8s resouces that are required to be available in the k8s cluster e.g. the editors referenced in each workspace. This is the first message agentk sends to rails right after agent starts/restarts/leader-election.
* `workspace_updates_full_resync` - Send the current state of all the workspaces in the k8s cluster. Rails will update postgres with the current state and respond with the workspaces to be created/updated/deleted in the k8s cluster and their last resource version that rails has persisted in postgres. Returning the persisted resource version back to agentk gives it a confirmation that the updates for that workspace have been successfully processed on rails end. This persisted resource version will also help with sending only the latest workspaces changes from agentk to rails for `workspace_updates` message. To keep things consistent between agentk and rails, agentk will send this message every time agent starts/restarts/leader-election after `prerequisites` message and after every `x` intervals of sending the `workspace_updates` message.
* `workspace_updates` - Send the latest workspace changes to rails that are not yet persisted in postgres. These latest changes will be calculated by keeping track of the persisted resource version of each workspace. Rails will update postgres with the current state  and respond with the workspaces to be created/updated/deleted in the k8s cluster and their last resource version that rails has persisted in postgres. Returning the persisted resource version back to agentk gives it a confirmation that the updates for that workspace have been successfully processed on rails end.

#### Rails API structure

There are multiple types of messages that Agentk can send to Rails. Having a single endpoint in Rails to handle all these different message types with different request/response structure is possible. Agentk, which is written in Go, can possibly handle this dynamic request/response structure with [RPC's `oneof` declaration](https://developers.google.com/protocol-buffers/docs/proto3#oneof). However, it would be easier to keep these message types as separate endpoints in Rails. This would allow better code clarity both on Rails and Agentk side, simpler OpenAPI sepcification, ease of adding new types of messages and ease of evolution of existing message type's request/response.

#### High level overview of polling architecture

```plantuml
!pragma teoz true

box gitlab monolith #Beige
participant rails order 20
box kas #Bisque
participant "kas" as kas order 40
end box
end box

box Kubernetes cluster #Beige
box agentk #Bisque
participant "agentk remote_dev\nmodule" as agentk_rd_mod order 50
end box
participant k8s order 60
end box


loop forever
  agentk_rd_mod -> k8s: Subscribe to k8s changes related to workspace
  activate agentk_rd_mod

  autoactivate on
  agentk_rd_mod -> kas: POST request with\nupdated workspace status/info
  note right
    Any updated workspace status/info from
    k8s is pushed with next poll.
  end note
  kas -> rails: proxy POST request from agentk to rails
  return Respond with any new work to achieve\nthe desired state of the workspaces
  return proxy workspace status\nactions to agentk
  autoactivate off

  agentk_rd_mod -> k8s: Apply any received workspace status actions to k8s
  deactivate agentk_rd_mod
end loop
```

#### Simplified example of applying a user-initiated request for workspace start

```plantuml
!pragma teoz true

actor user order 10
box gitlab monolith #Beige
participant rails order 20
participant postgres order 30
participant gitaly order 35
box kas #Bisque
participant "kas" as kas order 40
end box
end box

box Kubernetes cluster #Beige
box agentk #Bisque
participant "agentk remote_dev\nmodule" as agentk_rd_mod order 50
end box
participant k8s order 60
end box

autoactivate on

== user-initiated operation ==

group user-initiated request for work
user -> rails: request new workspace\nstart via GraphQL API
rails -> postgres: INSERT **workspace** record\ndesired_state: **started**
return record insert committed
return success response
end

group GA4K polling
group first poll

agentk_rd_mod -> agentk_rd_mod: first poll initiated

agentk_rd_mod -> kas: POST request to rails\nover gRPC connection to kas\n(empty, no workspace status to send)

kas -> rails: proxy POST request from agentk to rails 

rails -> gitaly: Retrieve additional non-persisted\nmetadata for work\n(e.g. via Gitaly API)
return metadata

rails -> rails: Internally generate YAML for\nKubernetes resources to create\n(e.g. DevWorkspace config)
return Generated YAML

return send workspace status action\n**desired_state=started**\nto kas in response body\nwith DevWorkpace config payload
return proxy workspace status\nactions to agentk

agentk_rd_mod -> k8s: use DevWorkspace Operator\nto create and start workspace

k8s -> k8s: k8s starts\nworkspace
return workspace started

return publish event for\nworkspace started

agentk_rd_mod -> agentk_rd_mod: Add status change to\nqueue for next poll
return status change queued
return first poll complete
end 'group first poll'

group second poll
agentk_rd_mod -> agentk_rd_mod: second poll initiated

agentk_rd_mod -> kas: POST request to rails\nover gRPC connection to kas\nwith **actual_state=started**\nfrom previous poll
kas -> rails: proxy POST request from agentk to rails
rails -> postgres: UPDATE workspace record\nactual_state: **started**
return record update committed
return success response\nindicating status was successfully updated
return send update indicating\nevent was successfully processed
return second poll complete
end 'group second poll'
end 'group GA4K polling'
rails -> user: Inform user of workspace status change
```


#### Detailed polling architecture

```plantuml
!pragma teoz true

box gitlab monolith #Beige
participant rails order 20
box kas #Bisque
participant "kas" as kas order 40
end box
end box

box Kubernetes cluster #Beige
box agentk #Bisque
participant "agentk remote_dev\nmodule" as agentk_rd_mod order 50
end box
participant k8s order 60
end box

autoactivate on

group GA4K polling

group k8s informer setup [run everytime agent starts/restarts/leader-election;\nruns forever]

agentk_rd_mod -> k8s: Subscribe to changes in workspace resources i.e. setup informer
return send informer info about\nworkspace created/updated/deleted

agentk_rd_mod -> agentk_rd_mod: update local information about the  config changes of the\nworkspaces that are created/updated/deleted
deactivate

end 'k8s informer setup'

group prerequisites poll [run everytime agent starts/restarts/leader-election]

agentk_rd_mod -> agentk_rd_mod: prerequisites poll initiated

agentk_rd_mod -> kas: POST request to rails of type\n"prerequisites"

kas -> rails: proxy POST request from agentk to rails

rails -> rails : Prerequisites Processing
deactivate
return send config to apply for components used\nin each workspace e.g. editor

return proxy work to agentk

agentk_rd_mod -> k8s: apply received k8s resources
return send info if resources were applied successfully or not

return prerequisites poll complete

end 'group prerequisites poll'


group workspace_updates_full_resync poll [run everytime agent starts/restarts/leader-election;\nruns after "prerequisites" poll;\nruns after every x intervals of sending the workspace_updates poll]

agentk_rd_mod -> agentk_rd_mod: workspace_updates_full_resync poll initiated

agentk_rd_mod -> agentk_rd_mod: generate the information for all workspace changes regardless\nof the last persisted resource version of that workspace
deactivate

agentk_rd_mod -> kas: POST request to rails of type\n"workspace_updates_full_resync"

kas -> rails: proxy POST request from agentk to rails

rails -> rails : Workspace Updates Full Resync Processing
deactivate

return send last persisted resource versionof all workspaces\nmanaged by agent and the config to apply for workspaces\nto be created/updated/deleted
return proxy work to agentk

agentk_rd_mod -> agentk_rd_mod: update local information about the last\npersisted resource version of all workspaces
deactivate

agentk_rd_mod -> k8s: apply received k8s resources
return send info if resources were applied successfully or not

return workspace_updates_full_resync poll complete

end 'group workspace_updates_full_resync poll'


group workspace_updates poll [runs forever]

agentk_rd_mod -> agentk_rd_mod: workspace_updates poll initiated

agentk_rd_mod -> agentk_rd_mod: generate the information for workspace changes that have\noccurred since the last persisted resource version of that workspace
deactivate

agentk_rd_mod -> kas: POST request to rails of type\n"workspace_updates"

kas -> rails: proxy POST request from agentk to rails 

rails -> rails : Workspace Updates Processing
deactivate

return send last persisted resource versionof all workspaces\nreceived by agent and the config to apply for workspaces\nto be created/updated/deleted
return proxy work to agentk

agentk_rd_mod -> agentk_rd_mod: update local information about the last\npersisted resource version of all workspaces which were sent to rails
deactivate

agentk_rd_mod -> k8s: apply received k8s resources
return send info if resources were applied successfully or not

return workspace_updates poll complete

end 'group workspace_updates poll'

end 'group GA4K polling'
```

#### Event driven polling and regular interval polling

While polling from agentk to rails at regular intervals achieves the goal of reconciliation of states, it is desirable to be able to tell agentk to not wait for the next polling period but instead poll immediately. Apart from making the architecture more event-driven it also helps to increase the interval between polls, thus reducing the load on the infrastructure.

With both of these mechanisms(event-driven and regular interval polling) in place, we have in place an "event based"/"edge-triggered" realtime loop, where bidirectional information requests/responses happen effectively immediately (or only limited by the processing/network time, not any additional polling sleep interval).

TODO: Update this description once https://gitlab.com/gitlab-org/gitlab/-/issues/387090 is implemented

### Rails Processing Logic

#### Message type: Prerequisites

TODO: Add more details and a diagram of this.

#### Message type:  Workspace Updates Full Resync

TODO: Add more details and a diagram of this.

#### Message type:  Workspace Updates

The following sequence diagram shows the high-level logic for the modules which implement the workspace updates processing
logic in Rails.

```plantuml
!pragma teoz true

actor ga4k order 10
box controller layer #Beige
participant "grape REST\nAPI" as api order 20
end box
box services layer #Beige
participant StateReconciliationService order 30
end box
box lib layer #Bisque
participant WorkspaceUpdatesProcessor order 40
participant ActualWorkspaceInfoParser order 50
participant DesiredConfigGenerator order 60
participant "ActiveRecord\n(A/R)" as ActiveRecord order 100
end box

autoactivate on

ga4k -> api: POST request
api -> StateReconciliationService: params from request
StateReconciliationService -> WorkspaceUpdatesProcessor: json array of actual\nworkspace infos from params

WorkspaceUpdatesProcessor -> WorkspaceUpdatesProcessor: iterate over json array of actual workspace infos
WorkspaceUpdatesProcessor -> ActualWorkspaceInfoParser: parse a single json workspace info
return ActualWorkspaceInfo object with unique name identifier
return array of ActualWorkspaceInfo objects

WorkspaceUpdatesProcessor -> WorkspaceUpdatesProcessor: iterate over array of ActualWorkspaceInfo objects
WorkspaceUpdatesProcessor -> ActiveRecord: load currently persisted Workspace objects by name
return array of Workspace objects
WorkspaceUpdatesProcessor -> WorkspaceUpdatesProcessor: update Workspace with new desired state if necessary (in RestartRequested case)
return
WorkspaceUpdatesProcessor -> ActiveRecord: update persisted Workspace object with new\ndesired state, actual state and resource versions
return
return

WorkspaceUpdatesProcessor -> ActiveRecord: load new Workspace objects with 'CreationRequested` desired state
return array of new Workspace objects

WorkspaceUpdatesProcessor -> WorkspaceUpdatesProcessor: Combine new Workspaces with existing Workspaces 
return combined array of new and existing Workspace objects

WorkspaceUpdatesProcessor -> WorkspaceUpdatesProcessor: iterate over complete array of Workspace objects
WorkspaceUpdatesProcessor -> DesiredConfigGenerator: Workspace object
return YAML for DevWorkspace + ConfigMap, representing config to apply for new\ndesired state, or nil if no updates needed. Note that this may be dependent upon\nlast updated timestamps, to retry configs which may have failed to apply.
return compacted array of workspace_rails_info compacted hashes\nrepresenting configs to apply
return tuple: either a payload\nor an error
return payload or error\nwrapped in\nServiceResponse
return POST response
```

### Workspace States

See https://github.com/devfile/api/blob/b39471be976b39a872e9e5d009382a74cb47f604/pkg/apis/workspaces/v1alpha2/devworkspace_types.go#L34
for the currently existing `DevWorkspacePhase` values. They are:

- `Starting`
- `Running`
- `Stopped`
- `Stopping`
- `Failed`
- `Error`

Also note that there are some **undocumented** phases that the DevWorkspaceOperator uses, specifically `Terminating`: https://github.com/devfile/devworkspace-operator/blob/a20ecb5934e865964b3d8dd34d6423e6e668a0db/controllers/workspace/status.go#L44-L44

TODO: Update these docs to reflect the `Terminating` state...

The states which are supported and persisted in the Rails DB for a `Workspace` record are identical to the `DevWorkspacePhase` values, with two additions:

1. The `CreationRequested` state to indicate the initial state of a Workspace
   before it has been submitted to the DevWorkspace Operator.
1. The `RestartRequested` state to indicate the user has requested a restart of the Workspace, but the
   restart has not yet successfully happened.

NOTE: We may also eventually want to incorporate information of the `DevWorkspaceConditionType` into the state logic
and/or Workspace UI status display: https://github.com/devfile/api/blob/b39471be976b39a872e9e5d009382a74cb47f604/pkg/apis/workspaces/v1alpha2/devworkspace_types.go#L61

#### Possible 'actual_state' values

The `actual_state` values are determined from the `phase` attribute in the k8s DevWorkspace changes
  which `agentk` listens to and sends to Rails.

The following diagram represents the normal flow of the `actual_state` values for a `Workspace` record based on the
`phase` values received from `agentk`. However, any of these states can be skipped if there have been any
transitional `phase` updates which were not received from `agentk` for some reason (a quick transition, a
failure to send the event, etc).

Additinally, the workspace can be in `Terminated` state which represents a deleted `Workspace`. This is triggered by Rails sending a `desired_state` of `Terminated`.

```plantuml

[*] --> CreationRequested
CreationRequested : Initial state before\nworkspace creation\nrequest is sent\nto kubernetes
CreationRequested -right-> Starting : phase=Starting

Starting : Workspace config is being\napplied to k8s 
Starting -right-> Running : phase=Running
Starting -down-> FailedOrError : phase=Failed|Error

Running : Workspace is running
Running -down-> Stopping : phase=Stopping
Running -down-> FailedOrError : phase=Failed|Error

Stopping : Workspace is stopping
Stopping -down-> Stopped : phase=Stopped
Stopping -left-> FailedOrError : phase=Failed|Error

Stopped : Workspace is Stopped\nby user request
Stopped -left-> FailedOrError : phase=Failed|Error
Stopped -down-> Terminated : desired_state == Terminated

Terminated: Workspace has been deleted

state "Failed | Error" as FailedOrError : Workspace is in a Failed or Error state.\nIt may be terminated, or sent a\nnew config and/or desired_state
FailedOrError -up-> Starting : phase=Starting
FailedOrError -right-> Stopped : phase=Stopped
```

#### Possible 'desired_state' values

The `desired_state` values are determined from the user's request to Rails, and are sent to `agentk` by Rails.

They are a subset of the `actual_state` values, only `Running`, `Stopped`, and `Terminated`. 
The state reconciliation logic in Rails will
continually attempt to transition the `actual_state` to the `desired_state` value, unless an unrecoverable failure/error
is reported by k8s. In this case, either an updated config or different `desired_state` value must be specified
to be applied to k8s.

There is also an additional supported state of `RestartRequested` which is only valid for `desired_state`.
This value is not a valid value for `actual_state`. It is required in order for Rails to
initiate a restart of a started workspace. It will only persist until a `phase` of `Stopped` is received
from `agentk`, indicating that the restart request was successful and in progress or completed.
At this point, the `desired_state`will be automatically changed to `Running` to trigger the workspace to restart again.
If there is a failure to restart the workspace, and a `Running` phase is never received, the
`desired_state` will remain `RestartRequested` until a new `desired_state` is specified.

```plantuml
[*] --> Running
Running : Workspace is running
Running -down-> Stopped : phase=Stopped
Running -left-> Terminated : phase=Terminated

Stopped : Workspace is Stopped\nby user request
Stopped -up-> Running : phase=Running
Stopped -down-> Terminated : phase=Terminated

Terminated: Workspace has been deleted

RestartRequested : User has requested a workspace restart.\n**desired_state** will automatically change\nto **'Running'** if actual state\nof Stopped is received.
RestartRequested -left-> Running : phase=Running

```

### 'agentk' calculation of workspace changes for workspace updates

When `agentk` listens to k8s changes for each workspace, it will keep a record of the resource versuib for the most recent
change for that workspace which has been successfully sent to rails and processed.

This is accomplished by sending the workspace resource-version to rails in the POST request, and Rails will echo them
back if the `actual_state` represented by the change were persisted successfully.

TODO: Update this description once https://gitlab.com/gitlab-org/gitlab/-/issues/387010 is implemented


### Rails 'actual_state' event timestamps

Rails will persist a timestamp of the most recent update to the `actual_state` field for each workspace.

This allows Rails to "retry" sending the `desired_state` to `agentk` if a workspace fails to be updated
to the `desired_state` in a timely manner.

This will allow for recovery if the initial update attempt fails, for example due to a network error somewhere
in the communication chain between `rails`, `kas`, `agentk`, or k8s.

TODO: Add more details and a diagram of this.

---

See https://gitlab.com/groups/gitlab-org/-/epics/9138#note_1174446349 for more details on **Challenges**, **Spike Progress**, and **Consequences**.

---
