# Cloud Sandbox Environment Setup

[[_TOC_]]

## Overview

The following are steps to create a development environment in AWS. Please note, this is not a production grade installation.

## Pre-requisities
* [eksctl](https://github.com/weaveworks/eksctl) (0.102.0)
* [aws](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-welcome.html) (aws-cli/2.7.31 Python/3.10.7 Darwin/21.6.0 source/arm64 prompt/off)
* [helm](https://github.com/helm/helm) (version.BuildInfo{Version:"v3.9.0", GitCommit:"7ceeda6c585217a19a1131663d8cd1f7d641b2a7", GitTreeState:"clean", GoVersion:"go1.18.2"})
* [helm-diff](https://github.com/databus23/helm-diff) (3.5.0)

```shell
export AWS_PROFILE=
export PROJECT_NAME=rd-group-mvc1
export SSH_PUBLIC_KEY_PATH=~/.ssh/gl_sandbox_group_remote_id_rsa.pub
```

## Create OIDC Application in Gitlab

* Browse to your GitLab instance's `User Profile -> Applications`
* Name: `$PROJECT_NAME-eks-oidc`
* Redirect URIs
    - http://localhost:8000/
    - http://localhost:18000/
* Scopes
    * openid
    * profile
    * email
* Export the Client ID and Client Secret
  ```shell
  export OIDC_ISSUER_URL=
  export OIDC_CLIENT_ID=
  export OIDC_CLIENT_SECRET=
  ```

## Create AWS Resources

### Create AWS Networking
* Browse the console to VPC section and click on `Create VPC`
* Select `VPC and more` in `Resources to create`
* Edit `Name tag auto-generation` to name of your project(`PROJECT_NAME`)
* Click on `Create VPC`
* This will create the following resources
    * 1 VPC
    * 2 public subnets and 2 private subnets
    * 1 route table for public subnets and 2 route tables for 1 private subnet in each availability zone
    * 1 Internet Gateway
    * 1 S3 endpoint
* Export the IDs of the created resources
  ```shell
  export VPC_ID=
  export SUBNET_ID_AZ1_PUBLIC=
  export SUBNET_ID_AZ2_PUBLIC=
  ```
* For each public subnet, set `Enable auto-assign public IPv4 address` in its setting

### Create security groups
* EKS Control plane Security Group
    * Name: `$PROJECT_NAME-eks-control-plane-sg`
    * VPC ID: `$VPC_ID`
    * Inbound rules: `none`
    * Outbound rules: `Allow all outbound traffic` (default)
    * Export the ID of the created resource
      ```shell
      export SECURITY_GROUP_ID_NODES=
      ```
* Nodes Security Group
    * Name: `$PROJECT_NAME-eks-nodes-sg`
    * VPC ID: `$VPC_ID`
    * Inbound rules: `Allow all outbound traffic`
    * Outbound rules: `Allow all outbound traffic` (default)
    * Export the ID of the created resource
      ```shell
      export SECURITY_GROUP_ID_NODES=
      ```

### Create AWS EKS cluster
* Use the template in `./config/eks-project-template.yaml` to create an EKS cluster
  ```shell
  cat "./config/eks-project-template.yaml" | envsubst > "./config/temp-eks-${PROJECT_NAME}.yaml"

  eksctl create cluster -f "./config/temp-eks-${PROJECT_NAME}.yaml"
  ```
* Fetch EKS kubeconfig
  ```shell
  aws eks update-kubeconfig --name "${PROJECT_NAME}"
  ```
* Get `OpenID Connect provider URL` from the EKS cluster
  ```shell
  EKS_OIDC_ISSUER_URL=$(aws eks describe-cluster --name "$PROJECT_NAME" --query "cluster.identity.oidc.issuer")

  export EKS_OIDC_ISSUER_URL=${EKS_OIDC_ISSUER_URL:1:-1}
  ```
* Create IAM Identity Provider
    * Browse to `IAM` > `Identity Providers` > `Add Provider`
    * Provider Type: `OpenID Connect`
    * Provider URL: $EKS_OIDC_ISSUER_URL
    * Audience: `sts.amazonaws.com`
* Set up the kubeconfig with OIDC
  ```shell
  # install kubectl oidc plugin - https://github.com/int128/kubelogin
  brew install int128/kubelogin/kubelogin

  # create clusterrolebinding for OIDC users
  kubectl create clusterrolebinding \
    "gitlab-${PROJECT_NAME}-eks-oidc-cluster-admin" \
    --clusterrole=cluster-admin \
    --user='https://gitlab.com#10764887' \
    --user='https://gitlab.com#1646689' \
    --user='https://gitlab.com#3457201' \
    --user='https://gitlab.com#4985794' \
    --user='https://gitlab.com#2233420' \
    --user='https://gitlab.com#9518928' \
    --user='https://gitlab.com#5212466'
    # --group='gid:gitlab-com/create-stage/editor'

  # set credentials for user
  kubectl config set-credentials "${PROJECT_NAME}-oidc" \
    --exec-api-version=client.authentication.k8s.io/v1beta1 \
    --exec-command=kubectl \
    --exec-arg=oidc-login \
    --exec-arg=get-token \
    --exec-arg=--oidc-issuer-url=${OIDC_ISSUER_URL} \
    --exec-arg=--oidc-client-id=${OIDC_CLIENT_ID} \
    --exec-arg=--oidc-client-secret=${OIDC_CLIENT_SECRET}

  # get cluster arn
  CLUSTER_ARN=$(
    aws eks describe-cluster \
      --name "$PROJECT_NAME" \
      --query "cluster.arn"
  )
  export CLUSTER_ARN=${CLUSTER_ARN:1:-1}

  # create context
  kubectl config set-context \
    "${PROJECT_NAME}-oidc" \
    --namespace=default \
    --cluster="${CLUSTER_ARN}" \
    --user="${PROJECT_NAME}-oidc"

  # switch context
  kubectl config use-context "${PROJECT_NAME}-oidc"

  # verify cluster access
  kubectl get nodes
  ```
* Install k8s-node-healthcheck
  ```shell
  export K8S_NODE_HEALTHCHECK_NAMESPACE=k8s-node-healthcheck
  kubectl create namespace "${K8S_NODE_HEALTHCHECK_NAMESPACE}"
  make k8s-node-healthcheck-helm-upgrade
  ```

### Create Application Load Balancer and route traffic to EKS cluster

```shell
# security group for application load balancer
SECURITY_GROUP_ID_ALB=$(
  aws ec2 create-security-group \
    --description "${PROJECT_NAME}-alb-sg" \
    --group-name "${PROJECT_NAME}-alb-sg" \
    --vpc-id "${VPC_ID}" \
    --query "GroupId"
)
export SECURITY_GROUP_ID_ALB=${SECURITY_GROUP_ID_ALB:1:-1}

# add inbound rule to security group
aws ec2 authorize-security-group-ingress \
  --group-id "${SECURITY_GROUP_ID_ALB}" \
  --protocol all \
  --cidr "0.0.0.0/0"

# create application load balancer
LOAD_BALANCER_ARN_APPLICATION=$(
  aws elbv2 create-load-balancer --name "${PROJECT_NAME}-alb" \
    --subnets ${SUBNET_ID_AZ1_PUBLIC} ${SUBNET_ID_AZ2_PUBLIC} \
    --security-groups ${SECURITY_GROUP_ID_ALB} \
    --scheme internet-facing \
    --type application \
    --query "LoadBalancers[0].LoadBalancerArn"
)
export LOAD_BALANCER_ARN_APPLICATION=${LOAD_BALANCER_ARN_APPLICATION:1:-1}

# create target group
TARGET_GROUP_ARN_HTTP_30080=$(
  aws elbv2 create-target-group \
    --name "${PROJECT_NAME}-tg-http-30080" \
    --protocol HTTP \
    --port 30080 \
    --target-type instance \
    --vpc-id "${VPC_ID}" \
    --health-check-protocol HTTP \
    --health-check-port 30333 \
    --query "TargetGroups[0].TargetGroupArn"
)
export TARGET_GROUP_ARN_HTTP_30080=${TARGET_GROUP_ARN_HTTP_30080:1:-1}

# get asg name of the eks node group
ASG_NAME_EKS_MANAGED_NODEGROUP_1=$(
  aws eks describe-nodegroup \
    --cluster-name "$PROJECT_NAME" \
    --nodegroup-name "$PROJECT_NAME-managed-ng-1" \
    --query "nodegroup.resources.autoScalingGroups[0].name"
)
export ASG_NAME_EKS_MANAGED_NODEGROUP_1=${ASG_NAME_EKS_MANAGED_NODEGROUP_1:1:-1}

# register auto scaling group with target group
aws autoscaling attach-load-balancer-target-groups \
  --auto-scaling-group-name ${ASG_NAME_EKS_MANAGED_NODEGROUP_1} \
  --target-group-arns ${TARGET_GROUP_ARN_HTTP_30080}

# create http listener on load balancer
aws elbv2 create-listener \
  --load-balancer-arn ${LOAD_BALANCER_ARN_APPLICATION} \
  --protocol HTTP \
  --port 80 \
  --default-actions Type=forward,TargetGroupArn=${TARGET_GROUP_ARN_HTTP_30080}

export ROUTING_DOMAIN=dw-poc.rd-group.gitlab.vishaltak.com
ACM_CERTIFICATE_ARN=$(
  aws acm request-certificate \
    --validation-method DNS \
    --domain-name ${ROUTING_DOMAIN} \
    --subject-alternative-names "*.${ROUTING_DOMAIN}" \
    --query "CertificateArn"
)
export ACM_CERTIFICATE_ARN=${ACM_CERTIFICATE_ARN:1:-1}

# manually verify the ownership of the domain in AWS ACM for certificate to becom operative

# create https listener on load balancer
aws elbv2 create-listener \
  --load-balancer-arn ${LOAD_BALANCER_ARN_APPLICATION} \
  --protocol HTTPS \
  --port 443 \
  --certificates CertificateArn=${ACM_CERTIFICATE_ARN} \
  --default-actions Type=forward,TargetGroupArn=${TARGET_GROUP_ARN_HTTP_30080}
```

<!---
### Create Network Load Balancer and route traffic to EKS cluster
```shell
# security group for network load balancer
SECURITY_GROUP_ID_NLB=$(
  aws ec2 create-security-group \
    --description "${PROJECT_NAME}-nlb-sg" \
    --group-name "${PROJECT_NAME}-nlb-sg" \
    --vpc-id "${VPC_ID}" \
    --query "GroupId"
)
export SECURITY_GROUP_ID_NLB=${SECURITY_GROUP_ID_NLB:1:-1}

# add inbound rule to security group
aws ec2 authorize-security-group-ingress \
  --group-id "${SECURITY_GROUP_ID_NLB}" \
  --protocol all \
  --cidr "0.0.0.0/0"

# create elastic ips
ELASTIC_IP_ALLOCATION_ID_NLB_1=$(
  aws ec2 allocate-address \
    --tag-specifications "ResourceType=elastic-ip,Tags=[{Key=Name,Value=${PROJECT_NAME}-nlb-eip1}]"
    --query "AllocationId"
)
export ELASTIC_IP_ALLOCATION_ID_NLB_1=${ELASTIC_IP_ALLOCATION_ID_NLB_1:1:-1}

ELASTIC_IP_ALLOCATION_ID_NLB_2=$(
  aws ec2 allocate-address \
    --tag-specifications "ResourceType=elastic-ip,Tags=[{Key=Name,Value=${PROJECT_NAME}-nlb-eip2}]" \
    --query "AllocationId"
)
export ELASTIC_IP_ALLOCATION_ID_NLB_2=${ELASTIC_IP_ALLOCATION_ID_NLB_2:1:-1}

# create network load balancer
LOAD_BALANCER_ARN_NETWORK=$(
  aws elbv2 create-load-balancer --name "${PROJECT_NAME}-nlb" \
    --subnet-mappings SubnetId=${SUBNET_ID_AZ1_PUBLIC},AllocationId=${ELASTIC_IP_ALLOCATION_ID_NLB_1} SubnetId=${SUBNET_ID_AZ2_PUBLIC},AllocationId=${ELASTIC_IP_ALLOCATION_ID_NLB_2} \
    --security-groups ${SECURITY_GROUP_ID_NLB} \
    --scheme internet-facing \
    --type network \
    --query "LoadBalancers[0].LoadBalancerArn"
)
export LOAD_BALANCER_ARN_NETWORK=${LOAD_BALANCER_ARN_NETWORK:1:-1}

# create target group
TARGET_GROUP_ARN_TCP_30090=$(
  aws elbv2 create-target-group \
    --name "${PROJECT_NAME}-tg-tcp-30090" \
    --protocol TCP \
    --port 30090 \
    --target-type instance \
    --vpc-id "${VPC_ID}" \
    --health-check-protocol HTTP \
    --health-check-port 30333 \
    --query "TargetGroups[0].TargetGroupArn"
)
export TARGET_GROUP_ARN_TCP_30090=${TARGET_GROUP_ARN_TCP_30090:1:-1}

# get asg name of the eks node group
ASG_NAME_EKS_MANAGED_NODEGROUP_1=$(
  aws eks describe-nodegroup \
    --cluster-name "$PROJECT_NAME" \
    --nodegroup-name "$PROJECT_NAME-managed-ng-1" \
    --query "nodegroup.resources.autoScalingGroups[0].name"
)
export ASG_NAME_EKS_MANAGED_NODEGROUP_1=${ASG_NAME_EKS_MANAGED_NODEGROUP_1:1:-1}

# register auto scaling group with target group
aws autoscaling attach-load-balancer-target-groups \
  --auto-scaling-group-name ${ASG_NAME_EKS_MANAGED_NODEGROUP_1} \
  --target-group-arns ${TARGET_GROUP_ARN_TCP_30090}

# create tcp listener on load balancer
aws elbv2 create-listener \
  --load-balancer-arn ${LOAD_BALANCER_ARN_NETWORK} \
  --protocol TCP \
  --port 443 \
  --default-actions Type=forward,TargetGroupArn=${TARGET_GROUP_ARN_TCP_30090}
```
--->

### Install ingress-nginx on EKS cluster

```shell
export INGRESS_NGINX_NODE_PORT_NAMESPACE=ingress-nginx-node-port
kubectl create namespace "${INGRESS_NGINX_NODE_PORT_NAMESPACE}"
# helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
# helm repo update
# helm fetch ingress-nginx/ingress-nginx --destination ./deploy/helm --untar
# mv ./deploy/helm/ingress-nginx ./deploy/helm/ingress-nginx-node-port
# Update the following values in ./deploy/helm/ingress-nginx-node-port/values.yaml
# - nameOverride
# - controller.config
# - controller.ingressClassByName
make ingress-nginx-node-port-helm-upgrade
```

### Configure Dependency - Cert Manager on EKS cluster

* Get `OpenID Connect provider URL` from the EKS cluster (If you've setup OIDC kube access mentioned above, this step is done)
  ```shell
  EKS_OIDC_ISSUER_URL=$(
    aws eks describe-cluster \
      --name "$PROJECT_NAME" \
      --query "cluster.identity.oidc.issuer"
  )
  export EKS_OIDC_ISSUER_URL=${EKS_OIDC_ISSUER_URL:1:-1}
  ```
* Create IAM Identity Provider (If you've setup OIDC kube access mentioned above, this step is done)
    * Browse to `IAM` > `Identity Providers` > `Add Provider`
    * Provider Type: `OpenID Connect`
    * Provider URL: $EKS_OIDC_ISSUER_URL
    * Audience: `sts.amazonaws.com`
* Create Route53 Public Hosted Zone for your domain
  ```shell
  export ROUTE53_HOSTED_ZONE_DOMAIN=
  export ROUTE53_HOSTED_ZONE_ID=
  ```
* Point the NS for `$ROUTE53_HOSTED_ZONE_DOMAIN` to the above created Hosted Zone's NS
* Create IAM Policy
  ```shell
  cat "./config/iam-policy-cert-manager-template.json" | envsubst > "./config/temp-iam-policy-cert-manager-${PROJECT_NAME}.json"

  IAM_POLICY_CERT_MANAGER_ARN=$(
    aws iam create-policy \
      --policy-name "${PROJECT_NAME}-cert-manager-policy" \
      --policy-document "file://./config/temp-iam-policy-cert-manager-${PROJECT_NAME}.json" \
      --query "Policy.Arn"
  )
  export IAM_POLICY_CERT_MANAGER_ARN=${IAM_POLICY_CERT_MANAGER_ARN:1:-1}
  ```
* Create IAM Role
  ```shell
  export EKS_OIDC_ISSUER_ID=${EKS_OIDC_ISSUER_URL/https:\/\//}

  AWS_ACCOUNT_ID=$(aws sts get-caller-identity --query "Account")
  export AWS_ACCOUNT_ID=${AWS_ACCOUNT_ID:1:-1}

  cat "./config/iam-role-trust-relationship-template.json" | envsubst > "./config/temp-iam-role-trust-relationship-cert-manager-${PROJECT_NAME}.json"

  IAM_ROLE_CERT_MANAGER_ARN=$(
    aws iam create-role \
      --role-name "${PROJECT_NAME}-cert-manager-irsa" \
      --assume-role-policy-document "file://./config/temp-iam-role-trust-relationship-cert-manager-${PROJECT_NAME}.json" \
      --query "Role.Arn"
  )
  export IAM_ROLE_CERT_MANAGER_ARN=${IAM_ROLE_CERT_MANAGER_ARN:1:-1}

  aws iam attach-role-policy --role-name "${PROJECT_NAME}-cert-manager-irsa" --policy-arn "${IAM_POLICY_CERT_MANAGER_ARN}"
  ```
* Install Cert Manager
  ```shell
  export CERT_MANAGER_NAMESPACE=cert-manager
  export CERT_MANAGER_GIT_TAG=v1.10.0
  kubectl create namespace "${CERT_MANAGER_NAMESPACE}"
  kubectl apply -f "https://github.com/cert-manager/cert-manager/releases/download/${CERT_MANAGER_GIT_TAG}/cert-manager.crds.yaml"
  # helm repo add jetstack https://charts.jetstack.io
  # helm repo update
  # helm fetch jetstack/cert-manager --version "${CERT_MANAGER_GIT_TAG}" --destination ./deploy/helm --untar
  # Update the following values in ./deploy/helm/cert-manager/values.yaml
  # - resources
  # - webhook.resources
  # - cainjector.resources
  # - startupapicheck.resources
  # - serviceAccount.annotations.eks.amazonaws.com/role-arn: ${IAM_ROLE_CERT_MANAGER_ARN}
  make cert-manager-helm-upgrade
  ```
* Add let's encrypt certificate issuer
  ```shell
  cat "./config/cert-manager-lets-encrypt-issuer-template.yaml" | envsubst > "./config/temp-cert-manager-lets-encrypt-issuer-${PROJECT_NAME}.yaml"

  kubectl apply -n "${CERT_MANAGER_NAMESPACE}" -f "./config/temp-cert-manager-lets-encrypt-issuer-${PROJECT_NAME}.yaml"
  ```
* Verify everything works
  ```shell
  helm repo add bitnami https://charts.bitnami.com/bitnami
  helm install dokuwiki bitnami/dokuwiki \
    --namespace default \
    --values ./config/dokuwiki-values.yaml
  # browse the URL of dokuwiki
  # remove dokuwiki
  helm delete dokuwiki
  ```

## Configure DevWorkspace Operator

Follow the steps mentioned above to [Install DevWorkspace Operator](#install-devworkspace-operator) and [Verify if everything is working](#verify-everything-is-working). Ensure you've set `ROUTING_SUFFIX` to your desired domain.
